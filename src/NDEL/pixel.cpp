//� 2018 NIREX ALL RIGHTS RESERVED

#include "pixel.h"

namespace NDEL
{
	Pixel::Pixel(unsigned char in_R, unsigned char in_G, unsigned char in_B, unsigned char in_A)
		: r(in_R)
		, g(in_G)
		, b(in_B)
		, a(in_A)
	{}

	unsigned char Pixel::GetR(void) const
	{
		return this->r;
	}

	unsigned char Pixel::GetG(void) const
	{
		return this->g;
	}

	unsigned char Pixel::GetB(void) const
	{
		return this->b;
	}

	unsigned char Pixel::GetA(void) const
	{
		return this->a;
	}

	void Pixel::SetR(unsigned char value)
	{
		this->r = value;
	}

	void Pixel::SetG(unsigned char value)
	{
		this->g = value;
	}

	void Pixel::SetB(unsigned char value)
	{
		this->b = value;
	}

	void Pixel::SetA(unsigned char value)
	{
		this->a = value;
	}

	unsigned char& Pixel::R(void)
	{
		return this->r;
	}

	unsigned char& Pixel::G(void)
	{
		return this->g;
	}

	unsigned char& Pixel::B(void)
	{
		return this->b;
	}

	unsigned char& Pixel::A(void)
	{
		return this->a;
	}

	Pixel Pixel::operator+(const Pixel& rhs) const
	{
		return Pixel(this->r + rhs.r, this->g + rhs.g, this->b + rhs.b, this->a + rhs.a);
	}

	Pixel Pixel::operator-(const Pixel& rhs) const
	{
		return Pixel(this->r - rhs.r, this->g - rhs.g, this->b - rhs.b, this->a - rhs.a);
	}

	Pixel Pixel::operator*(const Pixel& rhs) const
	{
		return Pixel(this->r * rhs.r, this->g * rhs.g, this->b * rhs.b, this->a * rhs.a);
	}

	Pixel Pixel::operator/(const Pixel& rhs) const
	{
		unsigned int denR = rhs.r;
		unsigned int denG = rhs.g;
		unsigned int denB = rhs.b;
		unsigned int denA = rhs.a;

		if (denR == 0) denR++;
		if (denG == 0) denG++;
		if (denB == 0) denB++;
		if (denA == 0) denA++;

		return Pixel(this->r / denR, this->g / denG, this->b / denB, this->a + denA);
	}

	Pixel Pixel::operator+=(const unsigned char& rhs)
	{
		R() += rhs;
		G() += rhs;
		B() += rhs;
		A() += rhs;
		return *this;
	}

	Pixel Pixel::operator-=(const unsigned char& rhs)
	{
		R() -= rhs;
		G() -= rhs;
		B() -= rhs;
		A() -= rhs;		
		return *this;
	}

	Pixel Pixel::operator*=(const unsigned char& rhs)
	{
		R() *= rhs;
		G() *= rhs;
		B() *= rhs;
		A() *= rhs;
		return *this;
	}

	Pixel Pixel::operator/=(const unsigned char& rhs)
	{
		if (rhs == 0)
		{
			return Pixel(0,0,0,0);
		}

		R() /= rhs;
		G() /= rhs;
		B() /= rhs;
		A() /= rhs;
		return *this;
	}

	Pixel Pixel::operator+=(const Pixel& rhs)
	{
		R() += rhs.r;
		G() += rhs.g;
		B() += rhs.b;
		A() += rhs.a;
		return *this;
	}

	Pixel Pixel::operator-=(const Pixel& rhs)
	{
		R() -= rhs.r;
		G() -= rhs.g;
		B() -= rhs.b;
		A() -= rhs.a;
		return *this;
	}

	Pixel Pixel::operator*=(const Pixel& rhs)
	{
		R() *= rhs.r;
		G() *= rhs.g;
		B() *= rhs.b;
		A() *= rhs.a;
		return *this;
	}

	Pixel Pixel::operator/=(const Pixel& rhs)
	{
		if (rhs.r != 0) R() /= rhs.r;
		if (rhs.g != 0) G() /= rhs.g;
		if (rhs.b != 0) B() /= rhs.b;
		if (rhs.a != 0) A() /= rhs.a;
		return *this;
	}
}
