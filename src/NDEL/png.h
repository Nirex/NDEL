#ifndef _N_PNG_H_
#define _N_PNG_H_

#include <string>
#include <vector>
#include "lodepng.h"
#include "matrix.h"
#include "pixel.h"
#include "channel.h"
#include "precompiled.h"

namespace NDEL
{
	class NAPI NPng final
	{
	PUBLIC:
		NPng(void);
		~NPng(void);

		int Load(std::string path);
		int Save(std::string path);

		NMatrix<Pixel>* GetMatrix(void) const;
		unsigned int Height(void) const;
		unsigned int Width(void) const;

		NMatrix<unsigned char> Channel(channel filter) const;

		NMatrix<unsigned char> RChannel(void) const;
		NMatrix<unsigned char> GChannel(void) const;
		NMatrix<unsigned char> BChannel(void) const;
		NMatrix<unsigned char> AChannel(void) const;

	PUBLICAUX:
		static inline int RawDataToPixel(std::vector<unsigned char> invec, int height, int width, NMatrix<Pixel>& outData);
		static inline std::vector<unsigned char> PixelToRawData(const NMatrix<Pixel>& inData);

	AUXILARY:
		int Decode(std::string filename);
		int Encode(std::string filename);
		int RawDataToPixel(std::vector<unsigned char> invec, int height, int width);
		std::vector<unsigned char> PixelToRawData(void);

		NMatrix<Pixel>* matImg;
	};

	using png = NPng;

	using PNG = NPng;

	using Png = NPng;
}

#endif // !_N_PNG_H_

