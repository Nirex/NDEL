//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_PIXEL_H_
#define _N_PIXEL_H_

#include "precompiled.h"

namespace NDEL
{
	class NAPI Pixel final
	{
	PUBLIC:
		Pixel(void) = default;
		Pixel(unsigned char in_R, unsigned char in_G, unsigned char in_B, unsigned char in_A);

		unsigned char GetR(void) const;
		unsigned char GetG(void) const;
		unsigned char GetB(void) const;
		unsigned char GetA(void) const;

		void SetR(unsigned char value);
		void SetG(unsigned char value);
		void SetB(unsigned char value);
		void SetA(unsigned char value);

		unsigned char& R(void);
		unsigned char& G(void);
		unsigned char& B(void);
		unsigned char& A(void);

		Pixel operator+(const Pixel& rhs) const;
		Pixel operator-(const Pixel& rhs) const;
		Pixel operator*(const Pixel& rhs) const;
		Pixel operator/(const Pixel& rhs) const;

		Pixel operator+=(const unsigned char& rhs);
		Pixel operator-=(const unsigned char& rhs);
		Pixel operator*=(const unsigned char& rhs);
		Pixel operator/=(const unsigned char& rhs);

		Pixel operator+=(const Pixel& rhs);
		Pixel operator-=(const Pixel& rhs);
		Pixel operator*=(const Pixel& rhs);
		Pixel operator/=(const Pixel& rhs);

	DATA:
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
	};
}
#endif // _N_PIXEL_H_
