# NDEL

Welcome to Nirex's Data Exchange Library

This library is created (or you know, being created as you read this!) to turn all types of data into matrices and matrices back into the data type you need.

The main purpose of this library is to allow Convolutional Neural Networks to work with all types of data through matrices (which is kinda what they do!), it is made to let data be gathered and converted more easily.

# STATUS

Under Active Development

# COPYRIGHT

MIT License

Copyright (c) 2018 Nirex0

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# COPYRIGHT NOTE!

This library (unlike others I've created) tends to use a lot of Third-Party files (Like Lodepng) and those Third-party files have their own respective copyright licenses/notices/terms of use, use of NDEL ALONE is under MIT license but I as the developer take absolutely no liability for misuse and/or unauthorized usage of third party applications used in NDEL in ways that do no obey the said third party copyright license/notice/terms of use.

# CONTACT

Nirex.0@gmail.com

# P.S

There are many many many data types out there in the real world and as a lone dev I will never be able to implement conversions for all of them in my lifetime, so feel free to help if you wish to!

# CREDITS

This section holds a link to all the libs that are being used in NDEL with their respective licenses and source pages.

|# |library|license|
|--|-------|-------|
|01|[lodepng](https://lodev.org/lodepng/)|[zlib](https://github.com/lvandeve/lodepng/blob/master/LICENSE)|
|02|[nanojpeg](http://keyj.emphy.de/nanojpeg/)|[MIT](http://svn.emphy.de/nanojpeg/trunk/nanojpeg/nanojpeg.c)|
|03|[TinyJPEG](https://github.com/serge-rgb/TinyJPEG/)|[public domain](https://github.com/serge-rgb/TinyJPEG/blob/master/tiny_jpeg.h)|
|04|[gif-h](https://github.com/ginsweater/gif-h/)|[public domain](https://github.com/ginsweater/gif-h/blob/master/LICENSE)|
|05|[gif_load](https://github.com/hidefromkgb/gif_load/)|[public domain](https://github.com/hidefromkgb/gif_load/blob/master/gif_load.h)|
