//� 2018 NIREX ALL RIGHTS RESERVED

#include "png.h"

namespace NDEL
{
	NPng::NPng(void)
	{
		matImg = new NMatrix<Pixel>();
	}

	NPng::~NPng(void)
	{
		delete matImg;
	}

	int NPng::Load(std::string path)
	{
		return Decode(path);
	}

	int NPng::Save(std::string path)
	{
		return Encode(path);
	}

	NMatrix<Pixel>* NPng::GetMatrix(void) const
	{
		return matImg;
	}

	unsigned int NPng::Height(void) const
	{
		return matImg->GetHeight();
	}

	unsigned int NPng::Width(void) const
	{
		return matImg->GetWidth();
	}

	NMatrix<unsigned char> NPng::Channel(channel filter) const
	{
		NMatrix<unsigned char> retMat(matImg->GetHeight(), matImg->GetWidth());

		if (filter == channel::Red)
		{
			for (int i = 0; i < matImg->GetHeight(); i++)
			{
				for (int j = 0; j < matImg->GetWidth(); j++)
				{
					retMat(i, j) = matImg->operator()(i, j).R();
				}
			}
		}
		else if (filter == channel::Green)
		{
			for (int i = 0; i < matImg->GetHeight(); i++)
			{
				for (int j = 0; j < matImg->GetWidth(); j++)
				{
					retMat(i, j) = matImg->operator()(i, j).G();
				}
			}
		}
		else if (filter == channel::Blue)
		{
			for (int i = 0; i < matImg->GetHeight(); i++)
			{
				for (int j = 0; j < matImg->GetWidth(); j++)
				{
					retMat(i, j) = matImg->operator()(i, j).B();
				}
			}
		}
		else if (filter == channel::Alpha)
		{
			for (int i = 0; i < matImg->GetHeight(); i++)
			{
				for (int j = 0; j < matImg->GetWidth(); j++)
				{
					retMat(i, j) = matImg->operator()(i, j).A();
				}
			}
		}
		else
		{
			retMat.Fill(0);
			return retMat;
		}
		return retMat;
	}

	NMatrix<unsigned char> NPng::RChannel(void) const
	{
		NMatrix<unsigned char> retMat(matImg->GetHeight(), matImg->GetWidth());

		for (int i = 0; i < matImg->GetHeight(); i++)
		{
			for (int j = 0; j < matImg->GetWidth(); j++)
			{
				retMat(i, j) = matImg->operator()(i, j).R();
			}
		}

		return retMat;
	}

	NMatrix<unsigned char> NPng::GChannel(void) const
	{
		NMatrix<unsigned char> retMat(matImg->GetHeight(), matImg->GetWidth());

		for (int i = 0; i < matImg->GetHeight(); i++)
		{
			for (int j = 0; j < matImg->GetWidth(); j++)
			{
				retMat(i, j) = matImg->operator()(i, j).G();
			}
		}

		return retMat;
	}

	NMatrix<unsigned char> NPng::BChannel(void) const
	{
		NMatrix<unsigned char> retMat(matImg->GetHeight(), matImg->GetWidth());

		for (int i = 0; i < matImg->GetHeight(); i++)
		{
			for (int j = 0; j < matImg->GetWidth(); j++)
			{
				retMat(i, j) = matImg->operator()(i, j).B();
			}
		}

		return retMat;
	}

	NMatrix<unsigned char> NPng::AChannel(void) const
	{
		NMatrix<unsigned char> retMat(matImg->GetHeight(), matImg->GetWidth());

		for (int i = 0; i < matImg->GetHeight(); i++)
		{
			for (int j = 0; j < matImg->GetWidth(); j++)
			{
				retMat(i, j) = matImg->operator()(i, j).A();
			}
		}

		return retMat;
	}

	inline int RawDataToPixel(std::vector<unsigned char> invec, int height, int width, NMatrix<Pixel>& outData)
	{
		if (invec.size() % 4 != 0)
		{
			return 1;
		}

		if (invec.size() != height * width)
		{
			return 2;
		}

		NMatrix<Pixel> mat(height, width);
		int LocInVec = 0;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				mat(i, j) = Pixel(invec[LocInVec], invec[LocInVec + 1], invec[LocInVec + 2], invec[LocInVec + 3]);
				LocInVec += 4;
			}
		}

		outData = mat;
		return 0;
	}

	inline std::vector<unsigned char> NPng::PixelToRawData(const NMatrix<Pixel>& inData)
	{
		std::vector<Pixel> pixVec;
		std::vector<unsigned char> retVec;

		pixVec = inData.ToVector();

		for (size_t i = 0; i < pixVec.size(); i++)
		{
			retVec.push_back(pixVec[i].R());
			retVec.push_back(pixVec[i].G());
			retVec.push_back(pixVec[i].B());
			retVec.push_back(pixVec[i].A());
		}

		return retVec;
	}

	int NPng::Decode(std::string filename)
	{
		std::vector<unsigned char> png;
		std::vector<unsigned char> image; 
		unsigned width, height;
		lodepng::State state; 

		unsigned error = lodepng::load_file(png, filename); 
		if (!error) error = lodepng::decode(image, width, height, state, png);


		if (error)
			return error;

		return RawDataToPixel(image, height, width);
	}

	int NPng::Encode(std::string filename)
	{
		std::vector<unsigned char> png;
		lodepng::State state;

		unsigned error = lodepng::encode(png, PixelToRawData(), Width(), Height(), state);
		if (!error) lodepng::save_file(png, filename);

		if (error)
			return error;
			
		return 0;
	}

	int NPng::RawDataToPixel(std::vector<unsigned char> invec, int height, int width)
	{
		if (invec.size() % 4 != 0)
		{
			return 1;
		}

		if (invec.size() != height * width)
		{
			return 2;
		}

		NMatrix<Pixel> mat(height, width);
		int LocInVec = 0;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				mat(i, j) = Pixel(invec[LocInVec], invec[LocInVec + 1], invec[LocInVec + 2], invec[LocInVec + 3]);
				LocInVec += 4;
			}
		}

		*matImg = mat;
		return 0;
	}

	std::vector<unsigned char> NPng::PixelToRawData(void)
	{
		std::vector<Pixel> pixVec;
		std::vector<unsigned char> retVec;

		pixVec = matImg->ToVector();

		for (size_t i = 0; i < pixVec.size(); i++)
		{
			retVec.push_back(pixVec[i].R());
			retVec.push_back(pixVec[i].G());
			retVec.push_back(pixVec[i].B());
			retVec.push_back(pixVec[i].A());
		}

		return retVec;
	}
}
