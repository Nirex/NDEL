//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_CHANNELS_H_
#define _N_CHANNELS_H_

#include "precompiled.h"

namespace NDEL
{
	enum class channel
	{
		InvalidChannel = 0,
		Red = 1,
		Green = 2,
		Blue = 3,
		Alpha = 4
	};
}
#endif // !_N_CHANNELS_H_
